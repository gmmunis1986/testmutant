var app = new Framework7({
  root: '#root_app',
  theme: 'aurora',
  name: 'Teste',
  id: 'br.com.teste',
  xhrCacheDuration: 0,
  cacheDuration: 0,
  xhrCache: false,
  reloadPages: false,
  sheet: {
    closeByBackdropClick: false,
    closeByOutsideClick: false,
  },
  lazy: {
    threshold: 50,
    sequential: true,
  },
  dialog: {
    title: 'Teste',
    buttonOk: 'Prosseguir',
    buttonCancel: 'Cancelar',
    animate: false,
    backdrop: true,
    color: 'blue',
  },
  view: {
    animate: true,
    pushState: false,
    masterDetailBreakpoint: 800,
    xhrCacheDuration: 10,
    cacheDuration: 10,
  },
  smartSelect: {
    closeOnSelect: true,
    popupCloseLinkText: 'Fechar',
    sheetCloseLinkText: 'Fechar',
    pageBackLinkText: 'Fechar',
  },
  touch: {
    fastClicks: true,
  },
  popup: {
    closeByBackdropClick: true,
  },
  input: {
    scrollIntoViewOnFocus: true,
  },
  popover: {
    backdrop: false,
  },
  toolbar: {
    hideOnPageScroll: true,
    showOnPageScrollEnd: true,
    showOnPageScrollTop: true,
  },
  smartSelect: {
    closeOnSelect: true,
  },
  routes: [{
    path: '/home/',
    url: 'index.html',
    name: 'home',
    cache: false,
  }]
});

let organiza = (data) => {
  data.sort(function (a, b) {
    return (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0);
  });
}

fetch('https://jsonplaceholder.typicode.com/users', {
  method: 'GET',
})
  .then((response) => response.json())
  .then((data) => {
    organiza(data);
    let SuitesArray = [];
    let AfabeticaArray = [];
    let WebsitesArray = [];
    data.map((item, index) => {
      if (item.address.suite) {
        SuitesArray.push({
          usuario: item.username,
          suitenome: item.address.suite
        });
      }
      AfabeticaArray.push({
        nome: item.name,
        email: item.email,
        company: item.company.name
      });

      WebsitesArray.push({
        user: item.username,
        site: item.website
      });
    });

    console.log('ALFABETICA: ' + JSON.stringify(AfabeticaArray))
    console.log('WEBSITES: ' + JSON.stringify(WebsitesArray))
    console.log('SUITES: ' + JSON.stringify(SuitesArray))

    app.virtualList.create({
      el: '.sites-lista',
      items: WebsitesArray,
      ul: '#linha_website',
      createUl: false,
      searchAll: function (query, items) {
        var found = [];
        for (var i = 0; i < items.length; i++) {
          if (items[i].site.toLowerCase().indexOf(query.toLowerCase()) >= 0 || query.trim() === '') found.push(i);
        }
        return found;
      },
      itemTemplate:
        '<li>' +
        '<a href="#" class="item-link item-content">' +
        '<div class="item-inner">' +
        '<div class="item-title-row">' +
        '<div class="item-title">{{user}}</div>' +
        '</div>' +
        '<div class="item-subtitle">{{site}}</div>' +
        '</div>' +
        '</a>' +
        '</li>',
      height: app.theme === 'ios' ? 63 : (app.theme === 'md' ? 73 : 46),
    });

    app.virtualList.create({
      el: '.alfabetica-lista',
      items: AfabeticaArray,
      ul: '#linha',
      createUl: false,
      searchAll: function (query, items) {
        var found = [];
        for (var i = 0; i < items.length; i++) {
          if (items[i].nome.toLowerCase().indexOf(query.toLowerCase()) >= 0 || query.trim() === '') found.push(i);
        }
        return found;
      },
      itemTemplate:
        '<li>' +
        '<a href="#" class="item-link item-content">' +
        '<div class="item-inner">' +
        '<div class="item-title-row">' +
        '<div class="item-title">{{nome}}</div>' +
        '</div>' +
        '<div class="item-subtitle">{{email}}</div>' +
        '<div class="item-text">{{company}}</div>' +
        '</div>' +
        '</a>' +
        '</li>',
      height: app.theme === 'ios' ? 63 : (app.theme === 'md' ? 73 : 46),
    });


    app.virtualList.create({
      el: '.suite-lista',
      items: SuitesArray,
      ul: '#linha_suite',
      createUl: false,
      searchAll: function (query, items) {
        var found = [];
        for (var i = 0; i < items.length; i++) {
          if (items[i].suitenome.toLowerCase().indexOf(query.toLowerCase()) >= 0 || query.trim() === '') found.push(i);
        }
        return found;
      },
      itemTemplate:
        '<li>' +
        '<a href="#" class="item-link item-content">' +
        '<div class="item-inner">' +
        '<div class="item-title-row">' +
        '<div class="item-title">{{usuario}}</div>' +
        '</div>' +
        '<div class="item-subtitle">{{suitenome}}</div>' +
        '</div>' +
        '</a>' +
        '</li>',
      height: app.theme === 'ios' ? 63 : (app.theme === 'md' ? 73 : 46),
    });


  })
  .catch((error) => {
    console.error(error);
  });